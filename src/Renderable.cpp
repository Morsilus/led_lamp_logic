#include "Renderable.h"

Renderable::Renderable(int pWidth, int pHeight) {
	renderTx.create(pWidth, pHeight);
}

Renderable::~Renderable() {
}

void Renderable::draw(sf::Drawable* pDrawable) {
	//renderTx.setActive(true);
	renderTx.draw(*pDrawable);
	renderTx.setActive(false);
}

void Renderable::clear() {
	renderTx.clear();
}

sf::Sprite* Renderable::getFrame() {
	renderTx.display();
	sf::Sprite* sprite = new sf::Sprite(renderTx.getTexture());
	return sprite; 
}
