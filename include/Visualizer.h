#ifndef VISUALIZER_H
#define VISUALIZER_H

#include <pthread.h>
#include <signal.h>
#include <stdint.h>

#include "types.h"

class Visualizer
{
	public:
		pthread_t thread;
		VisualizerData* vData;
	public:
		Visualizer(VisualizerData* pVData);
		~Visualizer();
		virtual void start() = 0;
		virtual void stop() = 0;
};

#endif // VISUALIZER_H
