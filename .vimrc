set tabstop=4
set softtabstop=4
set shiftwidth=4
set noexpandtab

set colorcolumn=96
highlight ColorColumn ctermbg=darkgray

let &path.="include/,src/,lib/,resources/,doc/,test/,spike/"

:syntax on
set autoindent
set smartindent
set confirm
set hidden
let g:ycm_confirm_extra_conf = 0
let g:ycm_autoclose_preview_window_after_completion = 1

let g:netrw_banner = 0
let g:netrw_liststyle = 3 
let g:netrw_winsize = 20
let g:netrw_browse_split = 0

augroup netrw 
autocmd!
autocmd BufRead,BufNewFile *.h,*.cpp set filetype=cpp.doxygen
augroup END

set makeprg=make\ -C\ .
nnoremap <F6> :make!<cr>
nnoremap <F7> :!./bin/runner<cr>
nnoremap <F8> :make!<cr> :!./bin/runner<cr>

nnoremap <F9> :make! spikes<cr> :!./bin/ticket<cr>
nnoremap <F10> :make! test<cr> :!./bin/tests<cr>

nnoremap <F5> :w<cr>
nnoremap <C-q> :qa<cr>
nnoremap <space> :Explore<cr>
nnoremap <tab> :bnext<cr>
nnoremap <s-tab> :bprev<cr>

colorscheme delek
set number
set numberwidth=4
set spell
set laststatus=2 
set so=7
set ruler
set cmdheight=1
set noerrorbells
set novisualbell
set history=500
set splitbelow
set splitright
