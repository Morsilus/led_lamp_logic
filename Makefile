CC := g++ 
SRCEXT := cpp
SRCDIR := src
BUILDDIR := build
TARGET_DIR := bin/
TARGET := bin/runner
CFLAGS := -g -Wall
LIB := -L libs -lpthread -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio 
TESTDIR := tests
TEST_MAIN := tests/test_main.cpp
TEST_TARGET := bin/tests

SOURCES := $(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))
TESTS := $(shell find $(TESTDIR) -type f -name *.$(SRCEXT) ! -path $(TEST_MAIN))
INC := -I include

all: link

build: $(OBJECTS) 

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(BUILDDIR)
	$(CC) $(CFLAGS) $(INC) -c -o $@ $<

link: $(TARGET)

$(TARGET): $(OBJECTS)
	@echo " Linking..."
	$(CC) $^ -o $(TARGET) $(LIB)

clean:
	@echo " Cleaning..."; 
	$(RM) -r $(BUILDDIR) $(TARGET) $(TEST_TARGET)

test: build	
	$(RM) $(TEST_TARGET)
	$(CC) tests/test_main.o $(filter-out %/main.o, $(OBJECTS)) $(TESTS) -o $(TEST_TARGET) $(LIB)

test_main:
	$(CC) tests/test_main.cpp -c -o tests/test_main.o

spikes:
	$(CC) $(CFLAGS) spike/ticket.cpp $(INC) $(LIB) -o bin/ticket

.PHONY: clean
