#ifndef RENDERER_H
#define RENDERER_H

#include <SFML/Graphics.hpp>
#include <stdio.h>
#include <pthread.h>
#include <vector>
#include <unistd.h>

#include "Renderable.h"

struct RendererData {
	sf::RenderWindow* window;
	std::vector<Renderable*>* renderables;
	int fps;
	pthread_mutex_t* mutex;
};

class Renderer 
{
    private:
        sf::RenderWindow* window;
		std::vector<Renderable*>* renderables;
		pthread_t renderThread;
		pthread_mutex_t mutex;
    public:
        Renderer(int pWidth, int pHeight, 
				int pFPS);
        virtual ~Renderer();

		void addRenderable(
				Renderable* pRenderable);
		void removeRenderable(
				Renderable* pRenderable);
    private:
        static void* render(void* pData);
};

#endif // RENDERER_H
