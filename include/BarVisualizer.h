#ifndef BARVISUALIZER_H
#define BARVISUALIZER_H

#include <SFML/Graphics.hpp>
#include <stdlib.h>
#include "types.h"

#include "Constants.h"
#include "DataProcessor.h"
#include "Visualizer.h"

enum FreqDividing {LIN, LOG};

class BarVisualizer : public Visualizer
{
    private:
		BarVisualizerData* barVisData;
	public:
        BarVisualizer(VisualizerData* pVData);
        virtual ~BarVisualizer();
		void start() override;
		void stop() override;
        void setNBars(int pNBars);
		bool setDividing(FreqDividing pDividing,
				double pMinF, double pMaxF);

		BarVisualizerData* getBarVisData();
    private:
		static void* run(void* pData);
};

#endif // BARVISUALIZER_H
