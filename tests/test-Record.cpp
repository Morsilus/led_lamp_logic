#include "catch.hpp"

#include "../include/Recorder.h"

#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

#define TEST

#ifdef TEST

TEST_CASE("Testing recording audio.") {
	AudioProcessorData* data = 
		(AudioProcessorData*) 
		malloc(sizeof(AudioProcessorData));

	pthread_mutex_t appMutex = 
		PTHREAD_MUTEX_INITIALIZER;
	pthread_cond_t appCond = 
		PTHREAD_COND_INITIALIZER;

	data->mutex = &appMutex;
	data->cond = &appCond;
	data->busy = true;
	data->magnitudes = (double*) malloc(512 * sizeof(double));

	Recorder recorder(data);
	recorder.start();

	pthread_mutex_lock(&appMutex);
	for (int i = 0; i < 10; i++) {
		while(data->busy) {
			printf("waiting\n");
			pthread_cond_wait(&appCond, 
					&appMutex);
		}
		data->busy = true;
		pthread_mutex_unlock(&appMutex);
	}
}

#endif