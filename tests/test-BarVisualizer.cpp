#include "catch.hpp"

#include "../include/BarVisualizer.h"
#include "../include/defs.h"
#include "../include/Recorder.h"

#include <pthread.h>

static void* printBarValues(void* pData);

TEST_CASE("Testing bar visualizer.", "[.]") {
	AudioProcessorData* apData = 
		(AudioProcessorData*) 
		malloc(sizeof(AudioProcessorData));

	pthread_mutex_t apMutex = 
		PTHREAD_MUTEX_INITIALIZER;
	pthread_cond_t apCond = 
		PTHREAD_COND_INITIALIZER;

	apData->mutex = &apMutex;
	apData->cond = &apCond;
	apData->busy = true;
	apData->magnitudes = (double*) malloc(512 * 
			sizeof(double));

	VisualizerData* visData = 
		(VisualizerData*) 
		malloc(sizeof(VisualizerData));

	pthread_mutex_t visMutex = 
		PTHREAD_MUTEX_INITIALIZER;
	pthread_cond_t visCond = 
		PTHREAD_COND_INITIALIZER;

	visData->mutex = &visMutex;
	visData->cond = &visCond;
	visData->busy = true;
	visData->apData = apData;

	Recorder recorder(apData);
	recorder.start();

	BarVisualizer barVisualizer(visData);
	barVisualizer.start();
	
	barVisualizer.setNBars(16);

	//pthread_t outputThread;
	//pthread_create(&outputThread, nullptr, printBarValues, barVisualizer.getBarVisData());

	sleep(5);
}

void* printBarValues(void* pData) {
	BarVisualizerData* data = (BarVisualizerData*) pData;
	while (true) {
		printf("\r");
		for (int i = 0; i < data->nBars; i++) {
			printf("%8.2f ", data->barsValues[i]);
		}
		fflush(stdout);
	}
}
