#ifndef RECORDER_H
#define RECORDER_H

#include <SFML/Audio.hpp>
#include <string.h>
#include <unistd.h>
#include <cmath>

// #include "AudioProcessor.h"
#include "DataProcessor.h"
#include "types.h"
#include "Constants.h"

class Recorder : public sf::SoundRecorder
{
    private:
		AudioProcessorData* data;
		double smoothing;
		double amplitude = 16000;
		double* weightings;
		double powerRatio;
		double bottomLimit;
		double topLimit;
    public:
        Recorder(AudioProcessorData* pData);
        virtual ~Recorder();
	private:
        virtual bool onProcessSamples(const sf::Int16* samples, std::size_t sampleCount) override;
};

#endif // RECORDER_H
