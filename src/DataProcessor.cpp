#include "DataProcessor.h"

DataProcessor::DataProcessor() {
}

DataProcessor::~DataProcessor() {
}
    
int DataProcessor::getFreqIndex(int nFft, double freq) {
    double freqUnit = ((double)MAX_FREQ / (nFft - 1));
    return (int) (freq / freqUnit);
}

int* DataProcessor::linearDivision(double min, double max, int n, int nAll) {
    int* indexes = (int*) malloc(n * sizeof(int));
	double unit = (max - min) / (n - 1);
    for (int i = 0; i < n; i++) {
        double freq = i * unit + min;
        indexes[i] = getFreqIndex(nAll, freq);
    }
    return indexes;
}

int* DataProcessor::logarithmicDivision(double min, double max, int n, int nAll) {
    int* indexes = (int*) malloc(n * sizeof(int));
    for (int i = 0; i < n; i++) {
        double freq = min * pow(max / min, ((double) i) / (n - 1));
        indexes[i] = getFreqIndex(nAll - n / max * MAX_FREQ, freq) + i;
    }
    return indexes;
}

double DataProcessor::arrayMax(double* array, int first, int last) {
	double max = .0;
	for (int i = first; i < last; i++) {
		if (array[i] > max) {
			max = array[i];
		}
	}
	return max;
}

double DataProcessor::arrayMean(double* array, int first, int last) {
	double mean = .0;
	for (int i = first; i < last; i++) {
		mean += array[i];
	}
	mean /= (last - first);
	return mean;
}

double DataProcessor::arrayWeightedMean(double* array, int first, int last, double* weights) {
}

double* DataProcessor::AWeighting(int n, double min, double max, double ratio) {
	double* weightings = (double*) malloc(n * sizeof(double));
	for (int i = 0; i < n; i++) {
		double freq = (max - min) / n * i + min;
		double freq2 = pow(freq, 2);
		weightings[i] = (20 * log10((pow(12194, 2) * pow(freq, 4)) / 
				((freq2 + pow(20.6, 2)) * (freq2 + pow(12194, 2)) * 
				sqrt((freq2 + pow(107.7, 2)) * (freq2 + pow(737.9, 2))))) + 2) * ratio;
		if (weightings[i] > 0) {
			weightings[i] = 0;
		}
		weightings[i] += 60; 
		weightings[i] /= 60;
	}
	return weightings;
}
