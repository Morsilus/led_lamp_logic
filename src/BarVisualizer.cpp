#include "BarVisualizer.h"

BarVisualizer::BarVisualizer(
		VisualizerData* pVData) : 
	Visualizer(pVData)
{
	barVisData = (BarVisualizerData*) malloc(sizeof(BarVisualizerData));
	barVisData->visData = vData;
	setNBars(N_BOXES);
	setDividing(DIVIDING, MIN_FREQ, MAX_FREQ);
}

BarVisualizer::~BarVisualizer()
{
    //dtor
}

void BarVisualizer::start() {
	pthread_create(&thread, nullptr, 
			run, barVisData);
	pthread_detach(thread);
}

void BarVisualizer::stop() {
	pthread_kill(thread, SIGSTOP);
}

void BarVisualizer::setNBars(int pNBars)
{
	barVisData->nBars = pNBars;
	barVisData->barsValues = (double*) malloc(barVisData->nBars * sizeof(double));
}

bool BarVisualizer::setDividing(FreqDividing pDividing, double pMinF, double pMaxF) {
	switch (pDividing) {
		case LIN:
			barVisData->dividers = DataProcessor::linearDivision(
					pMinF, pMaxF, barVisData->nBars + 1, N_FFT >> 1); 
			break;
		case LOG:
			barVisData->dividers = DataProcessor::logarithmicDivision(
					pMinF, pMaxF, barVisData->nBars + 1, N_FFT >> 1); 
			break;
	}
}

void* BarVisualizer::run(void* pData) {
	BarVisualizerData* barVisData = (BarVisualizerData*) pData;
	VisualizerData* visData = barVisData->visData;
	AudioProcessorData* apData = visData->apData;
	while (true) {
		while (apData->busy) {
			pthread_cond_wait(apData->cond, apData->mutex);
		}
		pthread_mutex_lock(visData->mutex);
		for (int i = 0; i < barVisData->nBars; i++) {
			barVisData->barsValues[i] = 
				//DataProcessor::arrayMax(apData->magnitudes, 
				//		barVisData->dividers[i], barVisData->dividers[i + 1]);
				DataProcessor::arrayMean(apData->magnitudes, 
						barVisData->dividers[i], barVisData->dividers[i + 1]);
			if (barVisData->barsValues[i] > 1) {
				barVisData->barsValues[i] = 1;
			}
		}
		visData->busy = false;
		pthread_cond_signal(visData->cond);
		pthread_mutex_unlock(visData->mutex);
		apData->busy = true;
		pthread_mutex_unlock(apData->mutex);
	}
	return 0;
}

BarVisualizerData* BarVisualizer::getBarVisData() {
	return barVisData;
}
