#include <unistd.h>

#include <SFML/Graphics.hpp>
        
const char* BAR_PATH = "resources/bar.png";

int main() {
	sf::RenderWindow app(sf::VideoMode(800, 600),
			"ha");
	app.display();

	sf::RenderTexture renderTx;
	renderTx.create(300, 300);
	
	sf::RectangleShape* bar  = 
		new sf::RectangleShape(sf::Vector2f(100, 100));
    bar->setPosition(sf::Vector2f(100 , 100));
	bar->setFillColor(sf::Color(0xFFFFFFFF));

	renderTx.draw(*bar);
	renderTx.display();
	app.draw(sf::Sprite(renderTx.getTexture()));

	app.display();
	sleep(3);

	//sf::RectangleShape 
	//	led(sf::Vector2f(5.f, 5.f));
	//led.setPosition(30, 50);
	//led.setFillColor(sf::Color(255, 0, 0, 255));

	//renderTx.draw(led);
	//renderTx.display();
	//sf::Sprite sprite(renderTx.getTexture());
	//app.draw(sprite);
	//app.display();
	//sleep(1);

	//led.setFillColor(sf::Color(0, 255, 0, 255));
	//renderTx.draw(led);
	//renderTx.display();
	//sf::Sprite sprite2(renderTx.getTexture());
	//app.draw(sprite2);
	//app.display();
	//sleep(3);
	return 0;
}
