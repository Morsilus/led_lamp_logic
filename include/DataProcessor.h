#ifndef DATA_PROCESSOR_H
#define DATA_PROCESSOR_H

#include <cstdlib>
#include <math.h>

#include "Constants.h"

class DataProcessor {
	private:
		DataProcessor();
		~DataProcessor();
	public:
	static int getFreqIndex(int nAll, double freq); 
	static int* linearDivision(double min, double max, int n, int nAll);
	static int* logarithmicDivision(double min, double max, int n, int nAll);
	static double arrayMax(double* array, int first, int last);
	static double arrayMean(double* array, int first, int last);
	static double arrayWeightedMean(double* array, int first, int last, double* weights);
	static double* AWeighting(int n, double min, double max, double ratio);
};

#endif // DATA_PROCESSOR_H
