#include "LEDStrip.h"

LEDStrip::LEDStrip(int pNLeds, int pWidth, 
		int pHeight) :
		Renderable(pWidth, pHeight) {
	int ledSize = pWidth / pNLeds;
	if (ledSize > pHeight) {
		ledSize = pHeight;
	}
	if (ledSize > 0) {
		nLeds = pNLeds;
		leds = (sf::RectangleShape**) 
			malloc(nLeds * 
					sizeof(sf::RectangleShape));
		for(int i = 0; i < nLeds; i++) {
			leds[i] = createLed(i, ledSize);
		}
	}	
}

LEDStrip::~LEDStrip() {
}

void LEDStrip::setLedColor(uint8_t pI, 
		uint32_t pColor) {
	if (pI < nLeds) {
		leds[pI]->setFillColor(sf::Color(pColor));
		draw(leds[pI]);
	}
}

sf::RectangleShape* LEDStrip::createLed(
		uint8_t pI, uint8_t pLedSize) {
	sf::RectangleShape* led = 
		new sf::RectangleShape(sf::Vector2f(
					pLedSize, pLedSize));
	led->setPosition(pI * pLedSize, 0);
	return led;
}
