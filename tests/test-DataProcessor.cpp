#include "catch.hpp"

#include "../include/DataProcessor.h"

#include <cstdlib>

void printIntArray(int* pArray, int pN);

TEST_CASE("Testing data processor.", "[.]") {
	int n = 17;
	int* array = (int*) malloc(n * sizeof(int));
	double min = 20;
	double max = 22050;

	printf("Parameters: n = %d, min = %.2f, max = %.2f\n", n, min, max);

	array = DataProcessor::linearDivision(min, max, n, 512);
	printf("Linear division: ");
	printIntArray(array, n);
	
	array = DataProcessor::logarithmicDivision(min, max, n, 512);
	printf("Logarithmic division: ");
	printIntArray(array, n);
	
	max = 11025;
	printf("Parameters: n = %d, min = %.2f, max = %.2f\n", n, min, max);
	
	array = DataProcessor::linearDivision(min, max, n, 512);
	printf("Linear division: ");
	printIntArray(array, n);
	
	array = DataProcessor::logarithmicDivision(min, max, n, 512);
	printf("Logarithmic division: ");
	printIntArray(array, n);
}

void printIntArray(int* pArray, int pN) {
	for (int i = 0; i < pN; i++) {
		printf("%d, ", pArray[i]);
	}
	printf("\n");
}
