#ifndef LEDSTRIP_H
#define LEDSTRIP_H

#include <SFML/Graphics.hpp>

#include "Renderable.h"

class LEDStrip : public Renderable 
{
	private:
		int nLeds = 0;
		sf::RectangleShape** leds;
	public:
		LEDStrip(int pNLeds, int pWidth, 
				int pHeight);
		virtual ~LEDStrip();

		void setLedColor(uint8_t pI, 
				uint32_t pColor);
	private:
		sf::RectangleShape* createLed(uint8_t pI, 
				uint8_t pLedSize);
};

#endif  // LEDSTRIP_H
