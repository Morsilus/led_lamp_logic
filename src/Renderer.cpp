#include "Renderer.h"

Renderer::Renderer(int pWidth, int pHeight,
		int pFPS) {	
    window = new sf::RenderWindow(
			sf::VideoMode(pWidth, pHeight), 
			"SFML window");
	renderables = new std::vector<Renderable*>();

	pthread_mutex_init(&mutex, nullptr);
	RendererData* data = (RendererData*)
		malloc(sizeof(RendererData));
	data->window = window;
	window->setActive(false);
	data->renderables = renderables;
	data->fps = pFPS;
	data->mutex = &mutex;
	pthread_create(&renderThread, nullptr,
			render, data);
	pthread_detach(renderThread);
}

Renderer::~Renderer() {
    window->close();
    //free(window);
	//pthread_exit(nullptr);
}

void Renderer::addRenderable(Renderable* pRenderable) {
	pthread_mutex_lock(&mutex);
	renderables->push_back(pRenderable);
	pthread_mutex_unlock(&mutex);
}

void Renderer::removeRenderable(
	Renderable* pRenderable) {
	int index = -1;
	for (int i = 0; i < (int)renderables->size();
			i++) {
		if (renderables->at(i) == pRenderable) {
			index = i;
		}
	}
	renderables->erase(renderables->begin() + 
			index);
}

void* Renderer::render(void* pData) {
	RendererData* data = (RendererData*) pData;
   
	data->window->setActive(true);
	data->window->setFramerateLimit(data->fps);
	while (data->window->isOpen()) {
        sf::Event event;
        while (data->window->pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                data->window->close();
        }

		pthread_mutex_lock(data->mutex);
		//data->window->setActive(true);
        data->window->clear();
		for (Renderable* renderable 
				: *(data->renderables)) {
			data->window->draw(*(renderable
					->getFrame()));
		}
        data->window->display();
		pthread_mutex_unlock(data->mutex);

		//usleep(1000000 / data->fps);
    }
	return nullptr;
}
