#ifndef CONSTANTS_H
#define CONSTANTS_H

#define N_BOXES 128 
#define DIVIDING LIN
#define MIN_FREQ 20
#define MAX_FREQ 22050
#define N_FFT 2048 

#endif //CONSTANTS_H