#include "Recorder.h"

Recorder::Recorder(AudioProcessorData* pData)
{
    setProcessingInterval(sf::milliseconds(20));
	data = pData;
	weightings = DataProcessor::AWeighting(N_FFT, 20, 22050, 1);
}

Recorder::~Recorder()
{
    stop();
}

bool Recorder::onProcessSamples(
		const sf::Int16* samples, 
		std::size_t sampleCount)
{
	//memset(vReal, 0, N_FFT);
	//memset(vImag, 0, N_FFT);
	//for (int i = 0; i < N_FFT; i++) {
	//	vReal[i] = samples[i];// * weightings[i];
	//	vImag[i] = 0;
	//}
	////FFT.Windowing(FFT_WIN_TYP_FLT_TOP, FFT_FORWARD);
	//FFT.Compute(FFT_FORWARD);
	//FFT.ComplexToMagnitude();
	////FFT.DCRemoval();
	//double mean = .0;
	//int nPeaks = 0;
	//double powerRatio = 10;
	//double powerRatio2 = powerRatio / 10;
	//double powerRatio3 = 10 / powerRatio;
	//for (int i = 0; i < (N_FFT >> 1); i++) {
	//	vReal[i] = (abs(vReal[i]) / amplitude) + data->magnitudes[i];
	//	//vReal[i] = pow(10., vReal[i] * 0.1) / 1 - 1;  
	//	vReal[i] = pow(10., vReal[i] / powerRatio) / powerRatio2 - powerRatio3;  
	//	if (vReal[i] > 1) {
	//		vReal[i] = 1;
	//		nPeaks++;
	//	}
	//	vReal[i] = vReal[i] * (1. - smoothing) + data->magnitudes[i] * smoothing;
	//	mean += vReal[i];
	//}
	//mean /= N_FFT;
	//int peak = (int) FFT.MajorPeak();
	//printf("%d  %f\n", (int)sampleCount, vReal[(int)peak] - mean);
	////if (vReal[peak] - mean > 0.1 || mean < 0.3) { //nPeaks > N_FFT / 50
	////	amplitude *= (1 + mean - 0.15);
	////} else if (vReal[peak] - mean < 0.1 || mean > 0.1) {
	////	amplitude *= (1 - 0.05);
	////}
	//
	//pthread_mutex_lock(data->mutex);
	//memcpy(data->magnitudes, vReal, N_FFT * sizeof(double));
	//data->busy = false;
	//pthread_cond_signal(data->cond);
	//pthread_mutex_unlock(data->mutex);
	return true;
}
