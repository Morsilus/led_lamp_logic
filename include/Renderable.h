#ifndef RENDERERABLE_H
#define RENDERERABLE_H

#include <SFML/Graphics.hpp>

class Renderable {
	private:
		sf::RenderTexture renderTx;
	public:
		Renderable(int pWidth, 
				int pHeight);
		virtual ~Renderable();
		sf::Sprite* getFrame();
	protected:
		void draw(sf::Drawable* pDrawable);
		void clear();
};

#endif // RENDERABLE_H
